package com.wemirr.platform.tools.service;

import com.wemirr.framework.boot.service.SuperService;
import com.wemirr.platform.tools.domain.entity.DynamicReleaseGrid;

/**
 * @author Levin
 */
public interface DynamicReleaseGridService extends SuperService<DynamicReleaseGrid> {
}
