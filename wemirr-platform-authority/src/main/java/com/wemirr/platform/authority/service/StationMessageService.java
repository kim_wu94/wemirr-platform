package com.wemirr.platform.authority.service;

import com.wemirr.framework.boot.service.SuperService;
import com.wemirr.platform.authority.domain.entity.message.StationMessage;

/**
 * @author Levin
 */
public interface StationMessageService extends SuperService<StationMessage> {


}
